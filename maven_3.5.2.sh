#!/bin/bash

wget http://ftp.byfly.by/pub/apache.org/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz

cp -r apache-maven-3.5.2-bin.tar.gz /opt/

cd /opt/

tar xvzf apache-maven-3.5.2-bin.tar.gz

echo 'export PATH="$PATH:/opt/apache-maven-3.5.2/bin"' >> /etc/profile

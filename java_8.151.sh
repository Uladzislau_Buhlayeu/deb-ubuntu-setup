#!/bin/bash

apt-get purge openjdk-\*

mkdir -p /usr/local/java

wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u151-b12/e758a0de34e24606bca991d704f6dcbf/jdk-8u151-linux-x64.tar.gz

cp -r jdk-8u151-linux-x64.tar.gz /usr/local/java

cd /usr/local/java

tar xvzf jdk-8u151-linux-x64.tar.gz

echo 'export  PATH="$PATH:/usr/local/java/jdk1.8.0_151/bin"' >> /etc/profile

update-alternatives --install "/usr/bin/javac" "javac" "/usr/local/java/jdk1.8.0_151/bin/javac" 1

update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/local/java/jdk1.8.0_151/bin/javaws" 1

update-alternatives --set javac /usr/local/java/jdk1.8.0_151/bin/javac

update-alternatives --set javaws /usr/local/java/jdk1.8.0_151/bin/javaws
